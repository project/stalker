<?php

/**
 * @file
 *
 * Menu callbacks for Stalker module.
 */
 
/**
 * Page callback function for user/%user/stalkers menu path
 */
function stalker_user_page($account) {
  if (variable_get('stalker_narcissism', TRUE)) {
    $sql = "SELECT stalker_uid, view_count FROM {stalker} WHERE profile_uid = %d AND view_count > %d ORDER BY view_count DESC";
  }
  else {
    $sql = "SELECT stalker_uid, view_count FROM {stalker} WHERE profile_uid = %d AND view_count > %d AND stalker_uid != profile_uid ORDER BY view_count DESC";
  }
  $result = db_query_range($sql, $account->uid, variable_get('stalker_threshold', 1), 0, 5);

  $items = array();

  while ($row = db_fetch_object($result)) {
    if (!variable_get('stalker_identity', FALSE)) {
      $viewer = user_load($row->stalker_uid);
      $items[] = theme('username', $viewer) . " : " . $row->view_count;
    }
    else {
      $items[] = variable_get('anonymous', t('Anonymous')) . ' : ' . $row->view_count;
    }
  }

  return theme('item_list', $items);
}






